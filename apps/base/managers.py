from django.db import models


class RemovedQuerySet(models.QuerySet):
    def removed(self):
        return self.filter(is_removed=False)

class RemovedManager(models.Manager):
    def get_queryset(self):
        return RemovedQuerySet(self.model, using=self._db)

    def removed(self):
        return self.get_queryset().removed()


