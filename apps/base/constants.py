from django.utils.translation import ugettext_lazy as _

GENDER = (
    ('M', _('Male')),
    ('F', _('Female'))
)