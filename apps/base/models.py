from django.db import models
from django.core.cache import cache
from django.utils.translation import ugettext_lazy as _
from .managers import RemovedManager


class TimeModel(models.Model):
    """ Modelo Base"""
    created = models.DateTimeField(_('created'), auto_now_add=True)
    modified = models.DateTimeField(_('modified'), auto_now=True)

    class Meta:
        abstract = True


class PositionModel(models.Model):
    """ Model for position """
    position = models.IntegerField(verbose_name=_('Position'), default=1)

    class Meta:
        verbose_name = _('Position Model')
        verbose_name_plural = _('Position Models')
        abstract = True
        ordering = ['position']


class RemovedModel(models.Model):
    is_removed = models.BooleanField(default=False)
    objects = RemovedManager()


    class Meta:
        verbose_name = "Delete Model"
        verbose_name_plural = "Delete Models"
        abstract = True

    def __str__(self):
        pass


class SingletonModel(models.Model):

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.pk = 1
        super(SingletonModel, self).save(*args, **kwargs)
        self.set_cache()

    @classmethod
    def load(cls):
        if cache.get(cls.__name__) is None:
            obj, created = cls.objects.get_or_create(pk=1)
            if not created:
                obj.set_cache()
        return cache.get(cls.__name__)

    def set_cache(self):
        cache.set(self.__class__.__name__, self.pk)


