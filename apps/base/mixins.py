from django.shortcuts import redirect, render
from django.contrib import messages
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin, PermissionRequiredMixin
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, UpdateView
from django.contrib.admin.utils import NestedObjects
from django.utils.text import capfirst
from django.utils.encoding import force_text


class TemplateNameMixin(object):

    MENU = ''
    SUB_MENU = ''

    def get_template_names(self):
        return ['administrador/' + self.template_name]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['MENU'] = self.MENU
        context['SUB_MENU'] = self.SUB_MENU
        return context

class LoginRequiredAdminMixin(LoginRequiredMixin, UserPassesTestMixin, TemplateNameMixin):
    login_url = reverse_lazy('admin_system:login')

    def test_func(self):
        return self.request.user.is_admin or self.request.user.is_superuser


class RedirectViewMixin:
    success_continue_url = ''
    success_add_url = ''

    def get_success_url(self):
        """Return the URL to redirect to after processing a valid form."""
        success_url = str(self.success_url)
        if self.request.POST.get('continue'):
            return reverse_lazy(self.success_continue_url, kwargs={'pk': self.object.pk})
        if self.request.POST.get('save'):
            return success_url
        if self.request.POST.get('another'):
            return self.success_add_url
        return success_url


class FilterListSelectionMixin:
    template_delete_name = ''
    is_removed = False
    danger_message = 'Se deben seleccionar elementos para poder realizar acciones sobre estos. No se han modificado elementos.'

    def get_danger_message(self):
        return self.danger_message

    def get_template_delete_name(self):
        return 'administrador/{0}'.format(self.template_delete_name)

    def post(self, request, *args, **kwargs):
        ctx = {}
        if request.POST.get('action') == 'delete_all':
            selection = request.POST.getlist('selection')
            if selection:
                queryset = self.get_queryset().filter(id__in=selection)
                ctx['queryset'] = queryset
            else:
                messages.danger(self.request, self.get_danger_message())
                return redirect(request.path)
        if request.POST.get('delete_selection') == 'si':
            queryset = self.get_queryset().filter(id__in=request.POST.getlist('list_item_delete'))
            if self.is_removed:
                queryset.update(is_removed=True)
            else:
                queryset.delete()
            return redirect(request.path)
        """
        Handle POST requests: instantiate a form instance with the passed
        POST variables and then check if it's valid.
        """
        return render(request, self.get_template_delete_name(), ctx)


class DeleteRelated(object):
    """d for ClassName"""
    def get_deleted_objects(self, objs):
        collector = NestedObjects(using='default')
        collector.collect(objs)

        def format_callback(obj):
            opts = obj._meta
            no_edit_link = '%s: %s' % (capfirst(opts.verbose_name),
                                       force_text(obj))
            return no_edit_link

        to_delete = collector.nested(format_callback)
        protected = [format_callback(obj) for obj in collector.protected]
        model_count = {model._meta.verbose_name_plural: len(objs) for model, objs in collector.model_objs.items()}
        return to_delete, model_count, protected

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        deletable_objects, model_count, protected = self.get_deleted_objects([self.get_object()])
        context['deletable_objects'] = deletable_objects
        context['model_count'] = dict(model_count).items()
        context['protected'] = protected
        return context