from django.shortcuts import render, redirect
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import ListView, UpdateView, CreateView, DeleteView
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.urls import reverse_lazy
from pure_pagination.mixins import PaginationMixin
from apps.base.mixins import LoginRequiredAdminMixin, RedirectViewMixin, DeleteRelated
from django.contrib.auth.models import Group, Permission
from .models import User
from .forms import UserCreateForm


class UserList(LoginRequiredAdminMixin, PaginationMixin, ListView):
    template_name = 'user/user_list.html'
    queryset = User.objects.all()
    MENU = 'USER'
    paginate_by = 25


class UserCreate(
        LoginRequiredAdminMixin, SuccessMessageMixin, RedirectViewMixin, CreateView):
    template_name = 'user/user_create_form.html'
    model = User
    form_class = UserCreateForm
    success_url = reverse_lazy('user:user_list')
    MENU = 'USER'
    success_message = 'Se modificó con éxito el usuario %(email)s '
    success_continue_url = 'user:user_update'
    success_add_url = reverse_lazy('user:user_create')


class UserUpdate(
        LoginRequiredAdminMixin, SuccessMessageMixin, RedirectViewMixin, UpdateView):
    template_name = 'user/user_update_form.html'
    model = User
    fields = ["email", "first_name", "last_name", "is_superuser"]
    success_url = reverse_lazy('user:user_list')
    MENU = 'USER'
    success_message = 'Se modificó con éxito el user %(email)s '
    success_continue_url = 'user:user_update'
    success_add_url = reverse_lazy('user:user_create')


class UserDelete(LoginRequiredAdminMixin,  DeleteRelated, DeleteView):
    template_name = 'user/user_delete.html'
    model = User
    success_url = reverse_lazy('user:user_list')
    MENU = 'USER'

    def delete(self, request, *args, **kwargs):
        return redirect(self.get_success_url())
