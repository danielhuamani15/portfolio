from django.urls import include, path
from .views import (UserList, UserCreate, UserUpdate, UserDelete)

urlpatterns = [
    path("usuario/", UserList.as_view(), name="user_list"),
    path("usuario/crear/", UserCreate.as_view(), name="user_create"),
    path("usuario/<int:pk>/", UserUpdate.as_view(), name="user_update"),
    path("usuario/<int:pk>/eliminar/", UserDelete.as_view(), name="user_delete"),
]

