from django.db import models
from uuslug import uuslug


class Tag(models.Model):
    name = models.CharField(max_length=255)
    slug = models.CharField(max_length=255, blank=True)

    class Meta:
        verbose_name = "Tag"
        verbose_name_plural = "Tags"

    def __str__(self):
        pass

    def save(self, *args, **kwargs):
        if self.name:
            self.slug = uuslug(self.name, instance=self, slug_field='slug',
                               filter_dict=None)
        super(Tag, self).save(*args, **kwargs)


class Category(models.Model):
    name = models.CharField(max_length=255)
    slug = models.CharField(max_length=255, blank=True)
    icon = models.ImageField(upload_to='category')

    class Meta:
        verbose_name = "Tag"
        verbose_name_plural = "Tags"

    def __str__(self):
        pass

    def save(self, *args, **kwargs):
        if self.name:
            self.slug = uuslug(self.name, instance=self, slug_field='slug',
                               filter_dict=None)
        super(Category, self).save(*args, **kwargs)


class Post(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    cover = models.ImageField(upload_to='post')
    title = models.CharField(max_length=255)
    content = models.TextField()
    tags = models.ManyToManyField('Tag', related_name='tags_set')
    author = models.ForeignKey(
        'users.User', related_name='user_posts', on_delete=models.CASCADE)
    category = models.ManyToManyField('Category', related_name='category_posts')
    slug = models.CharField(max_length=255, blank=True)
    post_relations = models.ManyToManyField('Post', related_name='post_relations_set')
    position = models.PositiveIntegerField(default=1)

    class Meta:
        verbose_name = "Blog"
        verbose_name_plural = "Blogs"

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.title:
            self.slug = uuslug(self.title, instance=self, slug_field='slug',
                               filter_dict=None)

        # self.stock_in_use = 0
        super(Post, self).save(*args, **kwargs)
