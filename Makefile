BUILD_COMPOSE_LOCAL_FILE := ./local.yml
NAME_PROJECT ?= 'portfolio'

docker-local:
	docker-compose -p $(NAME_PROJECT) -f $(BUILD_COMPOSE_LOCAL_FILE) up

docker-local-down:
	docker-compose -f $(BUILD_COMPOSE_LOCAL_FILE) down

docker-local-build:
	docker-compose -p $(NAME_PROJECT) -f $(BUILD_COMPOSE_LOCAL_FILE) build

docker-local-migrations:
	@ docker-compose -p $(NAME_PROJECT) -f $(BUILD_COMPOSE_LOCAL_FILE) run web python manage.py makemigrations --settings=config.settings.local

docker-local-migrate:
	@ docker-compose -p $(NAME_PROJECT) -f $(BUILD_COMPOSE_LOCAL_FILE) run web python manage.py migrate --settings=config.settings.local

docker-local-createuser:
	@ docker-compose -p $(NAME_PROJECT) -f $(BUILD_COMPOSE_LOCAL_FILE) run web python manage.py createsuperuser --settings=config.settings.local

startapp:
	@ docker-compose -p $(NAME_PROJECT) -f $(BUILD_COMPOSE_LOCAL_FILE) run web python manage.py startapp ${APP_NAME}
	@ sudo chown -R ${USER}:${USER} ${APP_NAME}
	@ mv ${APP_NAME} ./apps

style:
	sass --watch ./static/scss/main.scss:./static/css/main.css

