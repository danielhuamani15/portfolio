from .base import *

DEBUG = False

ALLOWED_HOSTS = []

STATIC_ROOT = ''
STATIC_URL = '/static/'

INSTALLED_APPS += [
]

MIDDLEWARE += [

]

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

MEDIA_ROOT = ''

MEDIA_URL = '/media/'

