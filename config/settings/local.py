from .base import *

DEBUG = True
ALLOWED_HOSTS = ["*"]

INTERNAL_IPS = ['127.0.0.1', 'localhost']
STATIC_ROOT = ''

STATIC_URL = '/static/'

INSTALLED_APPS += [
]

MIDDLEWARE += [
]

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

MEDIA_URL = '/media/'
