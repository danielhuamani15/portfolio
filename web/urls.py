from django.conf.urls import url
from .views.system import home, about
from .views.post import post, post_detail
from .views.course import courses

urlpatterns = [
    url(r'^$', home, name='home'),
    url(r'^post/$', post, name='post'),
    url(r'^cursos/$', courses, name='courses'),
    url(r'^post/detalle/$', post_detail, name='post_detail'),
    url(r'^sobre-mi/$', about, name='about'),
]


