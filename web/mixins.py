from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin, PermissionRequiredMixin


class LoginRequiredAdminMixin(
    LoginRequiredMixin, UserPassesTestMixin, TemplateNameMixin):
    login_url = reverse_lazy('admin_system:login')

    def test_func(self):
        return not self.request.user.is_admin