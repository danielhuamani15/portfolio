from django.shortcuts import render

def post(request):
    return render(request, 'post.html', locals())

def post_detail(request):
    return render(request, 'post_detail.html', locals())